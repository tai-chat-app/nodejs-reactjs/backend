import dbConnection from '../database';
import express, { Request, Response } from 'express'; // Import Request and Response types from express
import AuthController from '../app/controllers/AuthController';
const router = express.Router();
router.get('/all-user', (req: Request, res: Response) => {
    dbConnection.query("SELECT * FROM users", (err: Error, results: any) => {
        if (err) {
            console.error('error querying: ' + err.stack);
            res.status(500).json({ error: 'Database query error' });
            return;
        }
        // Convert results to JSON and send as a response
        res.status(200).json(results.rows);
    });
});

router.post('/create-user', AuthController.signup);
router.post('/signin', AuthController.signin)
router.post('/authenticate', AuthController.authenticate)

export default router;