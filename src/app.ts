import { server as WebSocketServer, connection as WebSocketConnection, request as WebSocketRequest, Message } from 'websocket';
import express from 'express'; // Import Request and Response types from express
import cors from 'cors';
import apiRouter from './routes/api';
const app = express();

const port = 8181;
app.use(cors());
/**
 * `app.use(express.json()); ` This line is so important, it can help controllers read the body of a request to fetch data and prevent error: TypeError: Cannot destructure property ... as it is undefined.
 * Source: https://stackoverflow.com/a/64155257
 * Explanation: https://www.educative.io/answers/what-is-the-expressjson-function
 * */ 
app.use(express.json()); 
app.use(apiRouter);


// Khởi động server
let server = app.listen(8181, () => {
    console.log('Server is running on port 8181');
});
// WebSocket server
const wsServer = new WebSocketServer({ httpServer: server });

interface Client extends WebSocketConnection {}

let clients: Client[] = [];

wsServer.on('request', (req: WebSocketRequest) => {
    const connection: Client = req.accept(null, req.origin);
    clients.push(connection);
    broadcastClientCount();

    connection.on('message', (msg: Message) => {
        if (msg.type === 'utf8') {
            const messageData = JSON.stringify({
                type: 'message',
                content: msg.utf8Data
            });
            for (const client of clients) {
                client.sendUTF(messageData);
            }
        }
    });

    connection.on('close', () => {
        // Remove client when connection is closed
        clients = clients.filter(c => c !== connection);
        broadcastClientCount();
    });
});

// Function to send current client count to all clients
function broadcastClientCount() {
    const message = JSON.stringify({
        type: 'count',
        total: clients.length
    });
    for (const client of clients) {
        client.sendUTF(message);
    }
}

