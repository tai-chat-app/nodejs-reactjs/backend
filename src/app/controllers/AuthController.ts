import { Request, Response } from 'express';
import dbConnection from '../../database';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
const secretKey = 'your_secret_key'; // Replace with your secret key
export default class AuthController{
    static async signup(req: Request, res: Response)
    {
        let { username, password, password2 } = req.body;
        username = username.trim();
        password = password.trim();
        password2 = password2.trim();
        // Kiểm tra xem body có dữ liệu không
        if (!username || !password || !password2) {
            res.status(400).json({ message: 'Lacks data' });
            return;
        }
    
        if (password !== password2) {
            res.status(400).json({ message: 'Passwords do not match' });
            return;
        }
        let hash = await bcrypt.hash(password,10);
        // Correct SQL syntax for PostgreSQL
        dbConnection.query("INSERT INTO users (username, password) VALUES ($1, $2)", [username, hash], (err: Error, results: any) => {
            if (err) {
                console.error('error inserting: ' + err.stack);
                res.status(500).json({ message: 'Database insert error' });
                return;
            }
    
            res.status(201).json({ message: 'User created successfully' });
        });
    }
    static signin(req: Request, res: Response){
        let {username, password}: {username: string, password:string} = req.body;
        username = username.trim();
        password = password.trim();
        if(username.length == 0 || password.length == 0)
        {
            res.status(400).json({message: 'Lacks data'});
        }
        else{
            dbConnection.query(`SELECT username FROM users WHERE username=$1`, [username], async (err:Error, result) =>{
                if (err) {
                    console.error('error select: ' + err.stack);
                    res.status(500).json({ message: 'Database select error' });
                }
                else if(result.rows.length == 0)
                {
                    res.status(401).send('The username or password is wrong');
                }
                else
                {
                    const match = await bcrypt.compare(password, result.rows[0].password);
                    if(match)
                    {
                        // User authenticated successfully, generate a JWT
                        const token = jwt.sign({ id: result.rows[0].id, username: result.rows[0].username }, secretKey, { expiresIn: '1h' });
                        res.status(200).json({ message: 'Signin is successful', object: result.rows, token: token });
                    }
                    else
                        res.status(401).send('The username or password is wrong');
                }
                    
            });

        }
    }
    static authenticate(req: Request, res: Response){
        console.log(req.headers);
        console.log(req.headers.authorization);
        
        const authHeader = req.headers['authorization'];
        const token = authHeader && authHeader.split(' ')[1];
    
        if (token == null) return res.status(401).json({ message: 'No token provided' });
    
        jwt.verify(token, secretKey, (err, user) => {
            if (err) return res.status(403).json({ message: 'Invalid token' });
            return res.status(200).json({ message: 'Valid token' });
        });
    }
}