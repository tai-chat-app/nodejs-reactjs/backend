import dotenv from 'dotenv';
import { Pool } from 'pg';

dotenv.config(); // Load environment variables from .env file

const connection = new Pool({
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: Number(process.env.DB_PORT) || 5432, // PostgreSQL default port
});

// Test the connection
connection.connect()
    .then(() => console.log('Connected to PostgreSQL'))
    .catch((err: Error) => console.error('Connection error', err.stack));

export default connection;
